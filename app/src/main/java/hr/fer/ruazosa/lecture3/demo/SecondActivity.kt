package hr.fer.ruazosa.lecture3.demo

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class SecondActivity : AppCompatActivity() {

    var firstNameEditTextView: EditText? = null
    var lastNameEditTextView: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.second_activity)

        firstNameEditTextView = findViewById<EditText>(R.id.first_name_text_edit)
        lastNameEditTextView = findViewById<EditText>(R.id.last_name_text_edit)

        //firstNameEditTextView?.setText(this.intent.getStringExtra("firstName"))
        //lastNameEditTextView?.setText(this.intent.getStringExtra("lastName"))
        firstNameEditTextView?.setText(FirstAndLastName.firstName)
        lastNameEditTextView?.setText(FirstAndLastName.lastName)

        val enterFistAndLastnameButton = findViewById<Button>(R.id.save_first_and_last_name_button)
        enterFistAndLastnameButton.setOnClickListener(
                { v: View ->
                    val resultActivity = Intent()
                    val firstName = firstNameEditTextView?.text.toString()
                    val lastName = lastNameEditTextView?.text.toString()

                    //resultActivity.putExtra("firstName", firstName)
                    //resultActivity.putExtra("lastName", lastName)
                    FirstAndLastName.firstName = firstName
                    FirstAndLastName.lastName = lastName
                    setResult(Activity.RESULT_OK, resultActivity)
                    finish()
                })
    }

}
