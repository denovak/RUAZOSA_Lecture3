package hr.fer.ruazosa.lecture3.demo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView


/**
 * Created by dejannovak on 20/03/2018.
 */
class HelloWordActivity: AppCompatActivity() {

    var firstNameTextView: TextView? = null
    var lastNameTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(R.layout.helloworld_activity)

        firstNameTextView = findViewById<TextView?>(R.id.first_name_text_view)
        lastNameTextView = findViewById<TextView?>(R.id.last_name_text_view)

        val enterFistAndLastnameButton = findViewById<Button>(R.id.enter_first_and_last_name_button)
        enterFistAndLastnameButton.setOnClickListener(
                { v: View ->
                    val startSecondActivityIntent = Intent(this, SecondActivity::class.java)
                    FirstAndLastName.firstName = firstNameTextView?.text.toString()
                    /*
                    startSecondActivityIntent.putExtra("firstName",
                            firstNameTextView?.text.toString())
                            */
                    FirstAndLastName.lastName = lastNameTextView?.text.toString()
                    /*
                    startSecondActivityIntent.putExtra("lastName",
                            lastNameTextView?.text.toString())*/

                    startActivityForResult(startSecondActivityIntent, 0)
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode) {
            Activity.RESULT_OK -> {
                //firstNameTextView?.text = data?.getStringExtra("firstName")
                //lastNameTextView?.text = data?.getStringExtra("lastName")
                firstNameTextView?.text = FirstAndLastName.firstName
                lastNameTextView?.text = FirstAndLastName.lastName

            }
        }
    }

}